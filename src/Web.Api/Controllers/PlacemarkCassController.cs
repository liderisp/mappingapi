﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Models;
using Web.Api.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlacemarkCassController : ControllerBase
    {
        private PlacemarkRepository placemarkRepository;

        public PlacemarkCassController()
        {
            this.placemarkRepository = new PlacemarkRepository();
        }

        [HttpPost]
        public async Task<IEnumerable<Placemark>> GetCounties([FromBody] FilteringModel filter = null)
        {
            var placemarks = await placemarkRepository.FindCountiesAsync("Latvia", filter);

            var placemarksWithSummaries = placemarks.Select(async p => await placemarkRepository.FindCountyWithChildSummariesAsync(p.Country, p.CountyId))
                                                                .Select(t => t.Result)
                                                                .Where(i => i != null)
                                                                .ToList();

            return placemarksWithSummaries;
        }

        [HttpPost("{countyId}")]
        public async Task<IEnumerable<Placemark>> GetParishes(string countyId, [FromBody] FilteringModel filter = null)
        {
            var placemarks = await placemarkRepository.FindParishesAsync("Latvia", countyId);

            if (placemarks.All(p => p.ParishId == p.CountyId))
            {
                return await placemarkRepository.FindSectorAsync("Latvia", countyId, countyId);
            }

            return placemarks
               .Select(async p => await placemarkRepository.FindParishWithChildSummariesAsync(p.Country, p.CountyId, p.ParishId))
               .Select(t => t.Result)
               .Where(i => i != null)
               .ToList();
        }

        [HttpPost("{countyId}/{parishId}")]
        public async Task<IEnumerable<Placemark>> GetSectors(string countyId, string parishId, [FromBody] FilteringModel filter = null)
        {
            var placemarks = await placemarkRepository.FindSectorAsync("Latvia", countyId, parishId);

            return placemarks;
        }
    }
}
