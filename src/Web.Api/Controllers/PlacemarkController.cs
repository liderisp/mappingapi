﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Models;
using Web.Api.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlacemarkController : ControllerBase
    {
        private IPlacemarkSqlRepository placemarkRepository;

        public PlacemarkController(IPlacemarkSqlRepository placemarkRepository)
        {
            this.placemarkRepository = placemarkRepository;
        }

        [HttpPost]
        public async Task<IEnumerable<Placemark>> PostGetCounties([FromBody] FilteringModel filter = null)
        {
            if (filter != null && filter.IsKmView)
            {
                return await placemarkRepository.GetSectorsAsync("Latvia", filter);
            }

            var placemarksWithSummaries = await placemarkRepository.GetCountiesWithChildSummariesAsync("Latvia", filter);

            return placemarksWithSummaries;
        }

        [HttpPost("{countyId}")]
        public async Task<IEnumerable<Placemark>> PostGetParishes(string countyId, [FromBody] FilteringModel filter = null)
        {
            if (filter != null && filter.IsKmView)
            {
                return await placemarkRepository.GetSectorsForCountyAsync("Latvia", countyId, filter);
            }

            var placemarks = await placemarkRepository.GetParishesWithChildSummariesAsync("Latvia", countyId);

            if (placemarks == null)
            {
                return await placemarkRepository.GetSectorsForParishAsync("Latvia", countyId, countyId, filter);
            }

            return placemarks;
        }

        [HttpPost("{countyId}/{parishId}")]
        public async Task<IEnumerable<Placemark>> PostGetSectors(string countyId, string parishId, [FromBody] FilteringModel filter = null)
        {
            var placemarks = await placemarkRepository.GetSectorsForParishAsync("Latvia", countyId, parishId, filter);

            return placemarks;
        }
    }
}
