﻿using Microsoft.EntityFrameworkCore;
using Web.Api.Models;

namespace Web.Api.DbContext
{
    public class NovitumContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<Placemark> PlaceMarks { get; set; }


        public NovitumContext(DbContextOptions<NovitumContext> options) : base(options)
        {

        }
    }
}
