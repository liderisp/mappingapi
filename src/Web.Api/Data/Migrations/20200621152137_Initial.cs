﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Api.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PlaceMarks",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CountyId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CountyName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParishId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParishName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SectorId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FieldValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ElderlyDensity = table.Column<int>(type: "int", nullable: true),
                    WorkAgeDensity = table.Column<int>(type: "int", nullable: true),
                    KidDensity = table.Column<int>(type: "int", nullable: true),
                    PotentialClientAmount = table.Column<int>(type: "int", nullable: true),
                    ClientAmount = table.Column<int>(type: "int", nullable: true),
                    Penetration = table.Column<float>(type: "real", nullable: true),
                    MaleDensity = table.Column<int>(type: "int", nullable: true),
                    Longitude = table.Column<double>(type: "float", nullable: true),
                    Lattitude = table.Column<double>(type: "float", nullable: true),
                    PopulationTotalAmount = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlaceMarks", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlaceMarks");
        }
    }
}
