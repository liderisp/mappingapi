﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Web.Api.DbContext;
using Web.Api.Models;
using Web.Api.Repositories;

namespace Web.Api
{
    public static class SeedTask
    {
        public static async Task LoadCounties()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            var optionBuilder = new DbContextOptionsBuilder<NovitumContext>();
            optionBuilder.UseSqlServer("Server=51.141.99.112;Initial Catalog=Novitum.Placemarks;User Id=sa2;Password=Pass@word");
            var context = new NovitumContext(optionBuilder.Options);
            var sqlRepository = new PlacemarkSqlRepository(context);

            var placeMarkList = new List<Placemark>();

            var countiesPoligons = DeserializeXMLFileToObject<kml>("SeedFiles/novadi_pol.kml");
            var parishPoligons = DeserializeXMLFileToObject<kml>("SeedFiles/pagasti_pol.kml");
            var kmPoligns = DeserializeXMLFileToObject<kml>("SeedFiles/1km_lv.kml");
            var listOfCountyParishMappings = File.ReadLines("SeedFiles/nov_pag_mappings.csv").Skip(1).Select(line => new Parish(line)).ToList();
            var listOfParish1KmMappings = File.ReadLines("SeedFiles/pagasta_km_mappings.csv").Skip(1).Select(line => new Poligon1km(line)).ToList();
            var listOf1KmFilterMappings = File.ReadLines("SeedFiles/1km_filtri.csv").Skip(1).Select(line => new Poligon1km(line)).ToList();

            foreach (var placemark in countiesPoligons.Document.Folder.Placemark)
            {
                var countyId = placemark.name;
                var countyParishMapping = listOfCountyParishMappings.FirstOrDefault(c => c.CountyCode == countyId);

                var county = new Placemark()
                {
                    Country = "Latvia",
                    CountyId = countyId,
                    ParishId = null,
                    SectorId = null,
                    CountyName = countyParishMapping.CountyName,

                    FieldValue = Serialize(placemark)

                };

                placeMarkList.Add(county);
            }

            foreach (var parish in listOfCountyParishMappings)
            {
                if (parish.ParishCode == parish.CountyCode)
                {
                    continue;
                }

                var placemarkFromKml =
                    parishPoligons.Document.Folder.Placemark.FirstOrDefault(p => p.name == parish.ParishCode);

                var placemark = new Placemark()
                {
                    Country = "Latvia",
                    CountyId = parish.CountyCode,
                    CountyName = parish.CountyName,
                    ParishId = parish.ParishCode,
                    SectorId = null,
                    ParishName = parish.ParishName,
                    FieldValue = placemarkFromKml == null ? null : Serialize(placemarkFromKml)
                };

                placeMarkList.Add(placemark);
            }

            foreach (var kmMapping in listOfParish1KmMappings)
            {
                var placemarkFromKml =
                    kmPoligns.Document.Folder.Placemark.FirstOrDefault(p => p.name == kmMapping.KmGridCode);
                var filterValue = listOf1KmFilterMappings.FirstOrDefault(f => f.KmGridCode == kmMapping.KmGridCode);
                var countyParishMapping = listOfCountyParishMappings.FirstOrDefault(c => c.ParishCode == kmMapping.ParishCode);

                var placemark = new Placemark()
                {
                    Country = "Latvia",
                    CountyId = countyParishMapping.CountyCode,
                    CountyName = countyParishMapping.CountyName,
                    ParishId = countyParishMapping.ParishCode,
                    ParishName = countyParishMapping.ParishName,
                    FieldValue = Serialize(placemarkFromKml),
                    SectorId = string.IsNullOrEmpty(placemarkFromKml.name) ? null : placemarkFromKml.name,

                    ClientAmount = filterValue.ClientAmount,
                    ElderlyDensity = filterValue.ElderlyDensity,
                    KidDensity = filterValue.KidDensity,
                    Lattitude = filterValue.Lattitude,
                    Longitude = filterValue.Longitude,
                    MaleDensity = filterValue.MaleDensity,
                    Penetration = filterValue.Penetration,
                    PopulationTotalAmount = filterValue.PopulationTotalAmount,
                    PotentialClientAmount = filterValue.PotentialClientAmount,
                    WorkAgeDensity = filterValue.WorkAgeDensity
                };
                placeMarkList.Add(placemark);
            }

            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("Build time: " + elapsedMs.ToString());

            var counter = 0;
            Console.WriteLine("Rows to add: " + placeMarkList.Count);
            watch = System.Diagnostics.Stopwatch.StartNew();
            foreach (var placemark in placeMarkList)
            {
                counter++;
                //  await repository.AddAsync(placemark);
            }


            watch.Stop();
            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("Seed time in cassandra: " + elapsedMs.ToString());

            if (context.PlaceMarks.Any())
            {
                return;
            }

            watch = System.Diagnostics.Stopwatch.StartNew();

            foreach (var placemark in placeMarkList)
            {
                counter++;
                await sqlRepository.AddAsync(placemark);
            }

            elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine("Seed time: " + elapsedMs.ToString());
        }

        public static T DeserializeXMLFileToObject<T>(string XmlFilename)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(XmlFilename)) return default(T);

            try
            {
                StreamReader xmlStream = new StreamReader(XmlFilename);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return returnObject;
        }

        public static string Serialize<T>(this T value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            try
            {
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add(string.Empty, string.Empty);

                var xmlserializer = new XmlSerializer(typeof(T));
                var stringWriter = new StringWriter();

                using var writer = XmlWriter.Create(stringWriter);
                xmlserializer.Serialize(writer, value, xmlns);
                return stringWriter.ToString()
                    .Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "")
                    .Replace("xmlns=\"http://www.opengis.net/kml/2.2\"", "")
                    .Replace("kmlDocumentFolderPlacemark", "Placemark");
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }
    }
}
