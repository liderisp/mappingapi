using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Web.Api.DbContext;
using Web.Api.Repositories;

namespace Web.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                        .SetIsOriginAllowed(host => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
            services.AddDbContext<NovitumContext>(options =>
            {
                options.UseSqlServer("Server=51.141.99.112;Initial Catalog=Novitum.Placemarks;User Id=sa2;Password=Pass@word",
                    sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(
                            typeof(NovitumContext).GetTypeInfo().Assembly.GetName().Name);
                        sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30),
                            errorNumbersToAdd: null);
                    });
            });

            services.AddScoped<IPlacemarkSqlRepository, PlacemarkSqlRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<NovitumContext>();
                context.Database.Migrate();

                var exceptions = new List<Exception>();

                for (int attempted = 0; attempted <= 15; attempted++)
                {
                    try
                    {
                        if (attempted > 0)
                        {
                            Thread.Sleep(5 * 1000);
                            SeedTask.LoadCounties().GetAwaiter().GetResult();
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("failed :" + ex.Message);
                        exceptions.Add(ex);
                    }
                }
            }
        }
    }

}
