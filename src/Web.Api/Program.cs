using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Web.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>


        Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    var configuration = new ConfigurationBuilder()
                        .AddJsonFile("appsettings.json", optional: true)
                        .AddJsonFile($"appsettings.{webBuilder.GetSetting("environment")}.json", optional: true).Build();
                    var applicationUrl = configuration["ApplicationUrl"];

                    webBuilder.UseStartup<Startup>()
                        .UseKestrel(options =>
                        {
                            options.Limits.MaxRequestBodySize = null;
                        })
                        .UseUrls("http://*:81;");
                    if (!string.IsNullOrEmpty(applicationUrl))
                        webBuilder.UseUrls(applicationUrl);
                });
    }
}
