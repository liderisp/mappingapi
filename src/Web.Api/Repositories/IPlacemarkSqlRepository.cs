﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Models;

namespace Web.Api.Repositories
{
    public interface IPlacemarkSqlRepository
    {
        Task AddAsync(Placemark data);
        Task<IEnumerable<Placemark>> GetCountiesWithChildSummariesAsync(string country, FilteringModel filter = null);
        Task<IEnumerable<Placemark>> GetParishesWithChildSummariesAsync(string country, string countyId, FilteringModel filter = null);
        Task<IEnumerable<Placemark>> GetSectorsForParishAsync(string country, string countyId, string parishId, FilteringModel filter = null);
        Task<IEnumerable<Placemark>> GetSectorsAsync(string country, FilteringModel filter = null);
        Task<IEnumerable<Placemark>> GetSectorsForCountyAsync(string country, string countyId, FilteringModel filter = null);
    }
}