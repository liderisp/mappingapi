﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.DbContext;
using Web.Api.Models;

namespace Web.Api.Repositories
{
    public class PlacemarkSqlRepository : IPlacemarkSqlRepository
    {
        private readonly NovitumContext context;

        public PlacemarkSqlRepository(NovitumContext context)
        {
            this.context = context;
        }
        public async Task AddAsync(Placemark data)
        {
            await context.PlaceMarks.AddAsync(data);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Placemark>> GetCountiesWithChildSummariesAsync(string country, FilteringModel filter = null)
        {
            var query = context.PlaceMarks.AsQueryable();

            var counties = await query.Where(p =>
               p.Country == country && p.ParishId == null && p.SectorId == null).ToListAsync();

            query = ApplyFilter(filter, query);

            var summaries = await query.GroupBy(p => p.CountyId).OrderBy(g => g.Key).Select(g =>
               new Placemark()
               {
                   CountyId = g.Key,
                   ClientAmount = g.Sum(p => p.ClientAmount),
                   PopulationTotalAmount = g.Sum(p => p.PopulationTotalAmount),
                   PotentialClientAmount = g.Sum(p => p.PotentialClientAmount),
               }
            ).ToListAsync();

            var result = summaries.Select(s =>
            {
                var placemark = counties.FirstOrDefault(p => p.CountyId == s.CountyId);
                placemark.ClientAmount = s.ClientAmount;
                placemark.PopulationTotalAmount = s.PopulationTotalAmount;
                placemark.PotentialClientAmount = s.PotentialClientAmount;

                return placemark;
            }).ToList();

            return result;
        }

        public async Task<IEnumerable<Placemark>> GetParishesWithChildSummariesAsync(string country, string countyId, FilteringModel filter = null)
        {
            var query = context.PlaceMarks.AsQueryable();

            var parishes = await query.Where(p =>
                 p.Country == country && p.CountyId == countyId && p.ParishId != null && p.SectorId == null).ToListAsync();

            query = ApplyFilter(filter, query);

            if (parishes.Count <= 0)
            {
                return null;
            }

            var summaries = await query.Where(p =>
                p.Country == country && p.CountyId == countyId && p.ParishId != null).GroupBy(p => p.ParishId).OrderBy(g => g.Key).Select(g =>
                 new Placemark()
                 {
                     ParishId = g.Key,
                     ClientAmount = g.Sum(p => p.ClientAmount),
                     PopulationTotalAmount = g.Sum(p => p.PopulationTotalAmount),
                     PotentialClientAmount = g.Sum(p => p.PotentialClientAmount)
                 }
            ).ToListAsync();

            var result = summaries.Select(s =>
            {
                var placemark = parishes.FirstOrDefault(p => p.ParishId == s.ParishId);
                placemark.ClientAmount = s.ClientAmount;
                placemark.PopulationTotalAmount = s.PopulationTotalAmount;
                placemark.PotentialClientAmount = s.PotentialClientAmount;

                return placemark;
            }).ToList();

            return result;
        }

        public async Task<IEnumerable<Placemark>> GetSectorsForParishAsync(string country, string countyId, string parishId, FilteringModel filter = null)
        {
            var query = context.PlaceMarks.Where(p =>
                p.Country == country && p.CountyId == countyId && p.ParishId == parishId && p.SectorId != null);

            query = ApplyFilter(filter, query);

            return await query.ToListAsync();
        }

        public async Task<IEnumerable<Placemark>> GetSectorsAsync(string country, FilteringModel filter = null)
        {
            var query = context.PlaceMarks.Where(p =>
                p.Country == country && p.CountyId != null && p.ParishId != null && p.SectorId != null);

            query = ApplyFilter(filter, query);

            return await query.ToListAsync();
        }
        public async Task<IEnumerable<Placemark>> GetSectorsForCountyAsync(string country, string countyId, FilteringModel filter = null)
        {
            var query = context.PlaceMarks.Where(p =>
                p.Country == country && p.CountyId == countyId && p.ParishId != null && p.SectorId != null);

            query = ApplyFilter(filter, query);

            return await query.ToListAsync();
        }


        private static IQueryable<Placemark> ApplyFilter(FilteringModel filter, IQueryable<Placemark> query)
        {
            if (filter != null)
            {
                if (!string.IsNullOrEmpty(filter.ElderlyDensity))
                {
                    var filterOptions = filter.ElderlyDensity.Split(",").Select(s => s.TryGetInt32())
                        .Where(n => n.HasValue)
                        .Select(n => n.Value)
                        .ToList();
                    query = query.Where(p => filterOptions.Count <= 0 || filterOptions.Count >= 3 || filterOptions.Contains(p.ElderlyDensity ?? -1));
                }

                if (!string.IsNullOrEmpty(filter.KidDensity))
                {
                    var filterOptions = filter.KidDensity.Split(",").Select(s => s.TryGetInt32())
                        .Where(n => n.HasValue)
                        .Select(n => n.Value)
                        .ToList();

                    query = query.Where(p => filterOptions.Count <= 0 || filterOptions.Count >= 3 || filterOptions.Contains(p.KidDensity ?? -1));
                }

                if (!string.IsNullOrEmpty(filter.WorkAgeDensity))
                {
                    var filterOptions = filter.WorkAgeDensity.Split(",").Select(s => s.TryGetInt32())
                        .Where(n => n.HasValue)
                        .Select(n => n.Value)
                        .ToList();

                    query = query.Where(p => filterOptions.Count <= 0 || filterOptions.Count >= 3 || filterOptions.Contains(p.WorkAgeDensity ?? -1));
                }

                if (filter.IsKmView)
                {
                    query = query.Where(p => !string.IsNullOrEmpty(p.SectorId));
                }

                if (filter.PenetrationFrom > 0 || filter.PenetrationTill > 0)
                {
                    var from = ((float)filter.PenetrationFrom / 100);
                    var till = ((float)filter.PenetrationTill / 100);
                    query = query.Where(p => p.Penetration >= from && p.Penetration <= till);
                }
            }

            return query;
        }

    }
}
