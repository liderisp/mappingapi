﻿using Cassandra;
using Cassandra.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Models;

namespace Web.Api.Repositories
{
    public class PlacemarkRepository
    {
        private ISession session;
        private Mapper mapper;
        const string TableName = "placemark";

        public PlacemarkRepository()
        {
            if (MappingConfiguration.Global.Get<Placemark>() == null)
            {
                MappingConfiguration.Global.Define(
                    new Map<Placemark>()
                        .TableName(TableName)
                        .PartitionKey(u => u.Country)
                        .ClusteringKey(u => u.CountyId)
                        .Column(u => u.Country, cm => cm.WithName("country"))
                        .Column(u => u.CountyId, cm => cm.WithName("county_id"))
                        .Column(u => u.CountyName, cm => cm.WithName("county_name"))
                        .Column(u => u.ParishId, cm => cm.WithName("parish_id"))
                        .Column(u => u.ParishName, cm => cm.WithName("parish_name"))
                        .Column(u => u.SectorId, cm => cm.WithName("sector_id"))
                        .Column(u => u.PopulationTotalAmount, cm => cm.WithName("km_total_population"))
                        .Column(u => u.Lattitude, cm => cm.WithName("lat"))
                        .Column(u => u.Longitude, cm => cm.WithName("long"))
                        .Column(u => u.MaleDensity, cm => cm.WithName("male_density_gr"))
                        .Column(u => u.KidDensity, cm => cm.WithName("kid_density_gr"))
                        .Column(u => u.WorkAgeDensity, cm => cm.WithName("work_age_density_gr"))
                        .Column(u => u.ElderlyDensity, cm => cm.WithName("elderly_density_gr"))
                        .Column(u => u.Penetration, cm => cm.WithName("penetration"))
                        .Column(u => u.ClientAmount, cm => cm.WithName("clients"))
                        .Column(u => u.PotentialClientAmount, cm => cm.WithName("potential"))
                        .Column(u => u.FieldValue, cm => cm.WithName("fieldValue")));
            }

            this.session = new Persistence.Cassandra.Connection().Connect();
            this.mapper = new Mapper(session);
        }
        public async Task AddAsync(Placemark data)
        {
            await mapper.InsertAsync(data, true, ttl: 0);
        }

        public Task<IEnumerable<Placemark>> FindCountiesAsync(string country, FilteringModel filter = null)
        {
            var selectStmt = "SELECT country, county_id, county_name, parish_id, parish_name, sector_id, km_total_population,lat,long,male_density_gr,kid_density_gr,work_age_density_gr,elderly_density_gr,penetration,clients,potential,blobAsText(fieldValue) AS fieldValue";
            var whereStmtm = " WHERE country = ? and county_id > ? and parish_id = ? ";
            return mapper.FetchAsync<Placemark>(selectStmt + $" FROM {TableName} " + whereStmtm + "  limit ? ALLOW FILTERING",
                country, "", "", 10000);
        }

        public async Task<Placemark> FindCountyWithChildSummariesAsync(string country, string countyId)
        {
            var selectStmt = "SELECT country, county_id, county_name, sum(km_total_population) as km_total_population, sum(clients) as clients, sum(potential) as potential,blobAsText(fieldValue) AS fieldValue";
            var whereStmtm = " WHERE country = ? and county_id = ? ";
            return await mapper.FirstAsync<Placemark>(selectStmt + $" FROM {TableName} " + whereStmtm + "  limit ? ALLOW FILTERING",
                country, countyId, 10000);
        }

        public Task<IEnumerable<Placemark>> FindParishesAsync(string country, string countyId)
        {
            var selectStmt = "SELECT country, county_id, county_name, parish_id, parish_name, sector_id, km_total_population,lat,long,male_density_gr,kid_density_gr,work_age_density_gr,elderly_density_gr,penetration,clients,potential,blobAsText(fieldValue) AS fieldValue";
            var whereStmtm = " WHERE country = ? and county_id = ? and parish_id > ? and sector_id = ? ";
            return mapper.FetchAsync<Placemark>(selectStmt + $" FROM {TableName} " + whereStmtm + "  limit ? ALLOW FILTERING",
                country, countyId, "", "", 10000);
        }

        public async Task<Placemark> FindParishWithChildSummariesAsync(string country, string countyId, string parishId)
        {
            var selectStmt = "SELECT country, county_id, county_name, parish_id, parish_name, sum(km_total_population) as km_total_population, sum(clients) as clients, sum(potential) as potential,blobAsText(fieldValue) AS fieldValue";
            var whereStmtm = " WHERE country = ? and county_id = ? and parish_id = ?";
            return await mapper.FirstAsync<Placemark>(selectStmt + $" FROM {TableName} " + whereStmtm + "  limit ? ALLOW FILTERING",
                country, countyId, parishId, 10000);
        }

        public Task<IEnumerable<Placemark>> FindSectorAsync(string country, string countyId, string parishId)
        {
            var selectStmt = "SELECT country, county_id, county_name, parish_id, parish_name, sector_id, km_total_population,lat,long,male_density_gr,kid_density_gr,work_age_density_gr,elderly_density_gr,penetration,clients,potential,blobAsText(fieldValue) AS fieldValue";
            var whereStmtm = " WHERE country = ? and county_id = ? and parish_id = ? and sector_id > ?";
            return mapper.FetchAsync<Placemark>(selectStmt + $" FROM {TableName} " + whereStmtm + "  limit ? ALLOW FILTERING",
                country, countyId, parishId, "", 10000);
        }
    }
}
