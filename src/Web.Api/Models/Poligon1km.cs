﻿using System;

namespace Web.Api.Models
{
    public class Poligon1km
    {
        public Poligon1km()
        {

        }
        public Poligon1km(string parishLine)
        {
            var split = parishLine.Split(',');

            if (split.Length <= 2)
            {
                //1km_kods, pagasta_kods
                KmGridCode = split[0];
                ParishCode = split[1];
            }
            else
            {
                KmGridCode = split[0];// 1km_kods
                PopulationTotalAmount = Convert.ToInt32(split[1]); //km_total_iedzivotaji double,
                Lattitude = Convert.ToDouble(split[2]); //lat double,
                Longitude = Convert.ToDouble(split[3]);  //long double,
                MaleDensity = DensityVariableValue(split[4]); //Male_Density_gr tinyint,
                KidDensity = DensityVariableValue(split[5]); //Kid_Density_gr tinyint,
                WorkAgeDensity = DensityVariableValue(split[6]); //Work_Age_Density_gr tinyint,
                ElderlyDensity = DensityVariableValue(split[7]);  //Elderly_Density_gr, tinyint
                Penetration = (float)Convert.ToDouble(split[8].Trim('"') + "." + split[9].Trim('"')); //Penetration float,
                ClientAmount = Convert.ToInt32(split[10]);//Clients double,
                PotentialClientAmount = Convert.ToInt32(split[11]);//Potencial double,
            }
        }

        public int ElderlyDensity { get; set; }
        public int WorkAgeDensity { get; set; }
        public int KidDensity { get; set; }
        public int PotentialClientAmount { get; set; }
        public int ClientAmount { get; set; }
        public float Penetration { get; set; }
        public int MaleDensity { get; set; }
        public double Longitude { get; set; }
        public double Lattitude { get; set; }
        public int PopulationTotalAmount { get; set; }
        public string KmGridCode { get; set; }
        public string ParishCode { get; set; }
        private static int DensityVariableValue(string value)
        {
            return value switch
            {
                "Low" => 1,
                "Medium" => 2,
                "High" => 3,
                _ => -1
            };
        }


    }
}
