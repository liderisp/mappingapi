﻿namespace Web.Api.Models
{
    public class FilteringModel
    {
        public string KidDensity { get; set; }
        public string WorkAgeDensity { get; set; }
        public string ElderlyDensity { get; set; }
        public bool IsKmView { get; set; }
        public int PenetrationFrom { get; set; }
        public int PenetrationTill { get; set; }
    }
}
