﻿using System;

namespace Web.Api.Models
{
    public class Placemark
    {
        public Guid Id { get; set; }
        public string Country { get; set; }
        public string CountyId { get; set; }
        public string CountyName { get; set; }
        public string ParishId { get; set; }
        public string ParishName { get; set; }
        public string SectorId { get; set; }
        public string FieldValue { get; set; }

        public int? ElderlyDensity { get; set; }
        public int? WorkAgeDensity { get; set; }
        public int? KidDensity { get; set; }
        public int? PotentialClientAmount { get; set; }
        public int? ClientAmount { get; set; }
        public float? Penetration { get; set; }
        public int? MaleDensity { get; set; }
        public double? Longitude { get; set; }
        public double? Lattitude { get; set; }
        public int? PopulationTotalAmount { get; set; }
    }
}
