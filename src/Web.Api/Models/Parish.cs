﻿namespace Web.Api.Models
{
    public class Parish
    {
        public Parish(string parishLine)
        {
            var split = parishLine.Split(',');

            //novada_kods,pagasts,pagasta_kods,novada_nosaukums
            CountyCode = split[0];
            ParishName = split[1];
            ParishCode = split[2];
            CountyName = split[3];
        }

        public string CountyName { get; set; }

        public string ParishCode { get; set; }

        public string ParishName { get; set; }

        public string CountyCode { get; set; }
    }
}
