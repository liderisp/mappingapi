﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Novitum.WebSpa
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var hostBuilder = new WebHostBuilder();
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true)
                .AddJsonFile($"appsettings.{hostBuilder.GetSetting("environment")}.json", optional: true).Build();
            var applicationUrl = configuration["ApplicationUrl"];

            var webHostBuilder = WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                    .UseUrls("http://*:80;");

            if (!string.IsNullOrEmpty(applicationUrl))
                return webHostBuilder.UseUrls(applicationUrl);

            return webHostBuilder;
        }
    }
}
