import { Injectable } from '@angular/core';
import { HttpEvent, HttpRequest, HttpResponse, HttpInterceptor, HttpHandler, HttpErrorResponse } from '@angular/common/http';

import { startWith, tap, catchError } from 'rxjs/operators';

import { Observable, of, throwError } from 'rxjs';
import { ErrorDialogService } from '../services/error-dialog.service';


@Injectable()
export class HttpCachingInterceptor implements HttpInterceptor {
    constructor(private errorDialogService: ErrorDialogService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        return this.sendRequest(req, next);
    }

    sendRequest(
        req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {
                let data = {};
                data = {
                    reason: error && error.message ? error.message : '',
                    status: error.status
                };
                this.errorDialogService.openDialog(data);
                return throwError(error);
            })
        );
    }
}
