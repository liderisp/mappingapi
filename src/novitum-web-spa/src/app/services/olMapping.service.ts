import OlMap from "ol/Map";
import Overlay from "ol/Overlay";
import OlVectorSource from "ol/source/Vector";
import OlVectorLayer from "ol/layer/Vector";
import OlView from "ol/View";
import OlFeature from "ol/Feature";
import OlPoint from "ol/geom/Point";
import OlXyzSource from "ol/source/XYZ";
import OlTileLayer from "ol/layer/Tile";
import { Icon, Style, Stroke, Fill } from "ol/style";
import KML from "ol/format/KML";

import { fromLonLat } from "ol/proj";
import { Subject } from "rxjs";
import { Placemark } from "../contracts";
import { Injectable } from "@angular/core";
import { Vector } from "ol/source";
import VectorSource from "ol/source/Vector";
import { PlacemarkService } from "./placemark.service";

@Injectable()
export class olMappingService {
  map: OlMap;
  vectorSource: OlVectorSource;
  vectorLayer: OlVectorLayer;
  xyzSource: OlXyzSource;
  tileLayer: OlTileLayer;
  view: OlView;
  displayValue: string;
  popup: Overlay;

  selectedPlacemark: any = new Subject();
  activePlacemark: any = new Subject();
  placemarkList: any = new Subject();

  parentId: string = null;
  urlToMap: string = "http://51.141.99.112:81/api/placemark";
  //urlToMap: string = "http://localhost:81/api/placemark";

  constructor(private placemarkService: PlacemarkService) {}

  createMap(mapContainerId: string) {
    this.vectorSource = new OlVectorSource({});

    this.vectorLayer = new OlVectorLayer({
      source: this.vectorSource,
    });

    this.xyzSource = new OlXyzSource({
      url: "http://tile.osm.org/{z}/{x}/{y}.png",
    });

    this.tileLayer = new OlTileLayer({
      source: this.xyzSource,
    });

    this.view = new OlView({
      center: fromLonLat([24.1052, 56.9496]),
      zoom: 8,
    });

    this.map = new OlMap({
      target: mapContainerId,
      layers: [this.tileLayer, this.vectorLayer],
      view: this.view,
    });

    var self = this;

    this.map.on("pointermove", function (evt) {
      var pixel = this.getEventPixel(evt.originalEvent);

      var hit = this.forEachFeatureAtPixel(pixel, function (feature, layer) {
        self.map.getTargetElement().style.cursor = "pointer";
        return true;
      });

      var feature = this.forEachFeatureAtPixel(evt.pixel, function (feature) {
        return feature;
      });

      if (feature) {
        var placemarkId = feature.values_.name;
        self.activePlacemark.next(placemarkId);

        self.popup.setPosition(evt.coordinate);
      } else if (self.popup) {
        self.popup.setPosition();
      }

      if (typeof hit === "undefined") {
        self.map.getTargetElement().style.cursor = "grab";
        return;
      }
    });

    this.map.on("singleclick", function (evt) {});

    var listenerStyle = new Style({
      stroke: new Stroke({
        color: "#ffff",
        width: 1,
      }),
      fill: new Fill({
        color: "rgba(255,0,0,0.1)",
      }),
    });

    this.map.on("click", function (event) {
      this.forEachFeatureAtPixel(event.pixel, function (feature, layer) {
        var polygon = feature.getGeometry();
        self.view.fit(polygon, { padding: [170, 50, 30, 150] });
        self.selectedPlacemark.next(feature.values_.name);

        feature.setStyle(listenerStyle);
      });
    });
  }

  loadVectorSourceForPlacemarks(kmlString): any {
    var features = new KML().readFeatures(kmlString, {
      dataProjection: "EPSG:4326",
      featureProjection: "EPSG:3857",
    });
    features.forEach((feature) => {
      feature.setStyle(
        new Style({
          stroke: new Stroke({
            color: "#000080",
            width: 1,
          }),
          fill: new Fill({
            color: "rgba(0, 0, 128,0.09)",
          }),
        })
      );
    });
    var source = this.vectorLayer.getSource();

    source.clear();
    source.addFeatures(features);
  }

  addFeaturePopUp(popUpContainerId: string) {
    var element = document.getElementById(popUpContainerId);
    this.popup = new Overlay({
      element: element,
      positioning: "bottom-center",
      offset: [5, -70],
      autoPan: true,
    });

    this.map.addOverlay(this.popup);
  }

  private setActiveMarker(markerId: string) {
    var marker = this.vectorSource.getFeatureById(markerId);

    if (marker) {
      this.view.setCenter(marker.getGeometry().getCoordinates());
    }
  }

  getInitialPlacemarkList(filter = null) {
    this.placemarkService
      .postGetCountyPlacemarks(filter)
      .subscribe((placemakrs) => {
        this.placemarkList.next(placemakrs);
        var placemarkKml =
          " <kml>" +
          placemakrs
            .map(function (p) {
              return p.fieldValue;
            })
            .join(",") +
          "</kml>";

        this.loadVectorSourceForPlacemarks(placemarkKml);
      });
  }

  getParishPlacemarks(countyId, filter = null) {
    this.placemarkService
      .postGetParishPlacemarks(countyId, filter)
      .subscribe((placemakrs) => {
        this.placemarkList.next(placemakrs);
        var placemarkKml =
          " <kml>" +
          placemakrs
            .map(function (p) {
              return p.fieldValue;
            })
            .join(",") +
          "</kml>";

        this.loadVectorSourceForPlacemarks(placemarkKml);
      });
  }
  getSectorPlacemarks(countyId, parishId, filter = null) {
    this.placemarkService
      .postGetSectorPlacemarks(countyId, parishId, filter)
      .subscribe((placemakrs) => {
        this.placemarkList.next(placemakrs);
        var placemarkKml =
          " <kml>" +
          placemakrs
            .map(function (p) {
              return p.fieldValue;
            })
            .join(",") +
          "</kml>";

        this.loadVectorSourceForPlacemarks(placemarkKml);
      });
  }
}
