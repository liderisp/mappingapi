import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Placemark, FilteringModel } from "../contracts";

const serverApi = "http://51.141.99.112:81/api/placemark";
//const serverApi = "http://localhost:81/api/placemark";
@Injectable()
export class PlacemarkService {
  headers: HttpHeaders = new HttpHeaders({
    "api-version": "1.0",
    "Content-Type": "application/json",
  });

  constructor(private http: HttpClient) {}

  postGetCountyPlacemarks(filter:FilteringModel  = null) {
    return this.http.post<Placemark[]>(serverApi, filter ||{}, {
      headers: this.headers,
    });
  }

  postGetParishPlacemarks(countyId, filter:FilteringModel  = null) {
    return this.http.post<Placemark[]>(serverApi + "/" + countyId, filter, {
      headers: this.headers,
    });
  }
  postGetSectorPlacemarks(countyId, parishId, filter:FilteringModel  = null) {
    return this.http.post<Placemark[]>(
      serverApi + "/" + countyId + "/" + parishId,
      filter ||{},
      { headers: this.headers }
    );
  }
}
