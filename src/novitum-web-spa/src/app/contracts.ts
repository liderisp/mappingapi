export interface Placemark {
  country: string;
  countyId: string;
  countyName: string;
  parishId: string;
  parishName: string;
  sectorId: string;
  fieldValue: string;
  elderlyDensity: number | null;
  workAgeDensity: number | null;
  kidDensity: number | null;
  potentialClientAmount: number | null;
  clientAmount: number | null;
  penetration: number | null;
  maleDensity: number | null;
  longitude: number | null;
  lattitude: number | null;
  populationTotalAmount: number | null;
}

export interface FilteringModel {
  kidDensity: string;
  workAgeDensity: string;
  elderlyDensity: string;
  isKmView: boolean;
  penetrationFrom: number;
  penetrationTill: number;
}
