import { Component, OnInit, AfterViewInit } from "@angular/core";
import { olMappingService } from "./services/olMapping.service";
import { Placemark, FilteringModel } from "./contracts";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit, AfterViewInit {
  title = "mapping";
  activePlacemark: Placemark;
  selectedCountyPlacemark: Placemark | undefined = undefined;
  selectedParishPlacemark: Placemark | undefined = undefined;
  popupPlacemark: Placemark | null;
  placemarkList: Placemark[] = [];
  filter: FilteringModel = {
    elderlyDensity: "",
    isKmView: false,
    kidDensity: "",
    workAgeDensity: "",
    penetrationFrom: 0,
    penetrationTill: 0,
  };

  constructor(private mappingService: olMappingService) {
    this.mappingService.activePlacemark.subscribe((id) => {
      this.activePlacemark = id;
      this.popupPlacemark = this.placemarkList.filter(
        (p) => p.countyId == id || p.parishId == id || p.sectorId == id
      )[0];
    });

    this.mappingService.placemarkList.subscribe((list) => {
      this.placemarkList = list;
    });
    this.mappingService.selectedPlacemark.subscribe((val) => {
      this.setUpMap(val);
    });

    this.mappingService.getInitialPlacemarkList();
  }

  private setUpMap(val: any) {
    if (this.filter.isKmView) {
      return;
    }

    if (!this.selectedCountyPlacemark) {
      this.selectedCountyPlacemark = this.placemarkList.filter(
        (p) => p.countyId == val
      )[0];
      this.mappingService.getParishPlacemarks(val, this.filter);
    } else if (!this.selectedParishPlacemark) {
      this.selectedParishPlacemark = this.placemarkList.filter(
        (p) => p.parishId == val
      )[0];
      this.mappingService.getSectorPlacemarks(
        this.selectedParishPlacemark.countyId,
        val,
        this.filter
      );
    }
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.mappingService.addFeaturePopUp("popup-container");
  }

  filterPlacemarks($event: any) {
    if (this.filter) {
      $event.isKmView = this.filter.isKmView;
    }

    this.filter = $event as FilteringModel;
    this.applyFilter();
  }

  setView(val) {
    this.filter.isKmView = val == "true";
    this.applyFilter();
  }

  private applyFilter() {
    if (!this.selectedCountyPlacemark) {
      this.mappingService.getInitialPlacemarkList(this.filter);
    } else if (!this.selectedParishPlacemark) {
      this.mappingService.getParishPlacemarks(
        this.selectedCountyPlacemark.countyId,
        this.filter
      );
    } else {
      this.mappingService.getSectorPlacemarks(
        this.selectedCountyPlacemark.countyId,
        this.selectedParishPlacemark.parishId,
        this.filter
      );
    }
  }

  resetDrillClick() {
    this.selectedCountyPlacemark = undefined;
    this.selectedParishPlacemark = undefined;
    this.mappingService.getInitialPlacemarkList(this.filter);
  }

  resetCountyClick() {
    this.selectedParishPlacemark = undefined;
    this.mappingService.getParishPlacemarks(
      this.selectedCountyPlacemark.countyId,
      this.filter
    );
  }
}
