import { NgModule } from "@angular/core";

import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatRippleModule } from "@angular/material/core";
import { MatDialogModule } from "@angular/material/dialog";
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from "@angular/material/form-field";
import { MatTableModule } from "@angular/material/table";
import { MatDividerModule } from "@angular/material/divider";
import {MatIconModule} from '@angular/material/icon';
import {
  MatButtonToggleModule
} from "@angular/material/button-toggle";
import {MatSliderModule} from '@angular/material/slider';

@NgModule({
  imports: [
    MatTableModule,
    MatRippleModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatDividerModule,
    MatButtonToggleModule,
    MatIconModule,
    MatSliderModule,
  ],
  exports: [
    MatTableModule,
    MatRippleModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatDividerModule,
    MatButtonToggleModule,
    MatIconModule,
    MatSliderModule,
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: "outline" },
    },
  ],
})
export class MaterialModule {}
