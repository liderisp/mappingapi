import { Component, OnInit, Input } from "@angular/core";
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
import { olMappingService } from "src/app/services/olMapping.service";
import { Placemark } from "src/app/contracts";

@Component({
  selector: "list-view",
  templateUrl: "./list-view.component.html",
  styleUrls: ["./list-view.component.scss"],
})
export class ListViewComponent implements OnInit {
  @Input() placemarkList: Placemark[];

  _showParish: boolean;
  get showParish(): boolean {
      return this._showParish;
  }
  @Input('showParish')
  set showParish(value: boolean) {
      this._showParish = value;
      this.setUpColumns();
  }

  _showSector: boolean;
  get showSector(): boolean {
      return this._showSector;
  }
  @Input('showSector')
  set showSector(value: boolean) {
      this._showSector = value;
      this.setUpColumns();
  }




  countyColumns = ["countyName"];
  parishColumns = ["parishName"];
  sectorColumns = ["sectorId"];
  dataColumns = ["population"];
  columnsToDisplay: string[] = [];

  constructor(private mappingService: olMappingService) {}

  ngOnInit() {

  }

  setUpColumns(){
    this.columnsToDisplay = [];
    this.columnsToDisplay = this.columnsToDisplay.concat(this.countyColumns);

    if (this.showParish) {
      this.columnsToDisplay = this.columnsToDisplay.concat(this.parishColumns);
    }

    if (this.showSector) {
      this.columnsToDisplay = this.columnsToDisplay.concat(this.sectorColumns);
    }

    this.columnsToDisplay = this.columnsToDisplay.concat(this.dataColumns);
  }

  setActivePlacemark(placemark: Placemark) {
    this.mappingService.selectedPlacemark.next(placemark);
  }
}
