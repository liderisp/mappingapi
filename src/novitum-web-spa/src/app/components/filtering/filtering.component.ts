import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { FilteringModel } from "src/app/contracts";
import { trigger, transition, animate, style } from "@angular/animations";

@Component({
  selector: "filtering",
  templateUrl: "./filtering.component.html",
  styleUrls: ["./filtering.component.scss"],
  animations: [
    trigger("slideInOut", [
      transition(":enter", [
        style({ transform: "translateY(-100%)" }),
        animate("200ms ease-in", style({ transform: "translateY(0%)" })),
      ]),
      transition(":leave", [
        animate("200ms ease-in", style({ transform: "translateY(-100%)" })),
      ]),
    ]),
  ],
})
export class FilteringComponent implements OnInit {
  @Output() applyFilter: EventEmitter<FilteringModel> = new EventEmitter<
    FilteringModel
  >();

  @Output()
  uploaded = new EventEmitter<string>();

  elderlyDensity: String;
  kidDensity: String;
  workAgeDensity: String;
  penetrationFrom: number;
  penetrationTill: number;

  visible: boolean = false;

  constructor() {}

  ngOnInit() {}

  resetFilterClick() {
    this.elderlyDensity = "";
    this.kidDensity = "";
    this.workAgeDensity = "";
    this.penetrationFrom=0;
    this.penetrationTill=0
  }

  expandToggleClick() {
    this.visible = !this.visible;
  }

  applyFilterClick() {
    this.applyFilter.emit({
      elderlyDensity: this.elderlyDensity + "",
      kidDensity: this.kidDensity + "",
      workAgeDensity: this.workAgeDensity + "",
      penetrationFrom:this.penetrationFrom,
      penetrationTill:this.penetrationTill
    } as FilteringModel);
  }

  formatLabel(value: number) {
    return value + "%";
  }
}
