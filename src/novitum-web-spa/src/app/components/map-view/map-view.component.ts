import { Component, OnInit } from '@angular/core';
import { olMappingService } from 'src/app/services/olMapping.service';
import { Placemark } from 'src/app/contracts';

@Component({
  selector: 'map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss']
})

export class MapViewComponent implements OnInit {

  constructor(private mappingService: olMappingService) {
  }

  ngOnInit() {
    this.mappingService.createMap("map");
  }
}