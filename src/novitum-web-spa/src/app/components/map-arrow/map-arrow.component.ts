import { Component, OnInit, Input } from "@angular/core";
import { Placemark } from "src/app/contracts";

@Component({
  selector: "map-arrow",
  templateUrl: "./map-arrow.component.html",
  styleUrls: ["./map-arrow.component.scss"],
})
export class MapArrowComponent implements OnInit {
  @Input() placemark: Placemark;

  constructor() {}

  ngOnInit() {}
  getDensityName(val) {
    switch (val) {
      case 1:
        return "Zems";
      case 2:
        return "Vidējs";
      case 3:
        return "Augsts";
    }
  }
}
