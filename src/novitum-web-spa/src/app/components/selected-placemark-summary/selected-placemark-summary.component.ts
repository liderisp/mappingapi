import { Component, OnInit, Input } from "@angular/core";
import { Placemark } from "src/app/contracts";

@Component({
  selector: "selected-placemark-summary",
  templateUrl: "./selected-placemark-summary.component.html",
  styleUrls: ["./selected-placemark-summary.component.scss"],
})
export class SelectedPlacemarkSummaryComponent implements OnInit {
  @Input() placemark: Placemark;

  constructor() {}

  ngOnInit() {}
  getDensityName(val) {
    switch (val) {
      case 1:
        return "Zems";
      case 2:
        return "Vidējs";
      case 3:
        return "Augsts";
    }
  }
}
