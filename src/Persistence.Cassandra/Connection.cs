﻿using Cassandra;

namespace Persistence.Cassandra
{
    public class Connection
    {
        public Connection()
        {

        }

        public void CreateKeyspace()
        {
            var cluster = Cluster.Builder().AddContactPoint("cassandra").Build();
            var session = cluster.Connect();//
            session.Execute("CREATE KEYSPACE IF NOT EXISTS geo_places WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };");
        }

        public void CreateTable()
        {
            using (var session = this.Connect())
            {
                session.Execute($@"
                    CREATE TABLE IF NOT EXISTS geo_places.placemark (
                        country varchar, 
                        county_id varchar, 
                        county_name varchar, 
                        parish_id varchar, 
                        parish_name varchar, 
                        sector_id varchar, 
                        km_total_population int,
                        lat double,
                        long double,
                        male_density_gr int,
                        kid_density_gr int,
                        work_age_density_gr int,
                        elderly_density_gr int,
                        penetration float,
                        clients int,
                        potential int,
                        fieldValue blob,
                        primary key ((country), county_id, parish_id, sector_id)
                        ) WITH CLUSTERING ORDER BY ( county_id ASC);
            	");
            }
        }

        public ISession Connect()
        {
            string[] nodes = "cassandra".Split(',');

            Cluster cluster = Cluster.Builder()
                .AddContactPoints(nodes)
                .WithDefaultKeyspace("geo_places")
                .WithCredentials("User", "Password")
                .Build();

            return cluster.Connect("geo_places");
        }
    }
}
